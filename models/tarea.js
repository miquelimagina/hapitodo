console.log('tarea.js');

var Datastore = require('nedb');
var db = {};
db.tareas = new Datastore({ filename: __dirname + '/db.json', autoload: true });

var Joi = require('joi');

var tareaSchema = Joi.object().keys({
    title:        Joi.string().alphanum().min(3).max(70).required(),
    description:  Joi.string().alphanum().min(3).max(140).required(),
    status:       Joi.string().required().valid(['todo', 'doing', 'review','done']),
    tags:         Joi.array().items(Joi.string().required()),
    //createdBy:    Joi.date().max('now'),
    createdAt  :  Joi.date().max('now'),
    updateddAt  : Joi.date().max('now'),
});


/**
 * Getters
 */
var getTags = function (tags) {
  return tags.join(',');
};

/**
 * Setters
 */
var setTags = function (tags) {
  return tags.split(',');
};
/*
function getTareas(request,reply){
  return reply('get tareas');
}*/



function getTareas(request) {

    if (request.query.name) {
        request.reply(findTareas(request.query.name));
    }
    else {
        request.reply(tareas);
    }
}


function findTareas(name) {
    return tareas.filter(function(tarea) {
        return tarea.name.toLowerCase() === name.toLowerCase();
    });
}

function getProduct(request) {
    var tarea = tareas.filter(function(p) {
        return p.id == request.params.id;
    }).pop();

    request.reply(tarea);
}

function addProduct(request) {
    var tarea = {
        id: tareas[tareas.length - 1].id + 1,
        name: request.payload.name
    };

    tareas.push(tarea);

    request.reply.created('/tareas/' + tarea.id)({
        id: tarea.id
    });
}


var tareas = [{
        id: 1,
        name: 'Guitar'
    },
    {
        id: 2,
        name: 'Banjo'
    }
];

/*
function getTareas(request){
  if (request.query.title) {
    request.reply(findProducts(request.query.title));
  }else {
    tareas=
    request.reply(products);
  }

}

function getProducts(request) {

    if (request.query.name) {
        request.reply(findProducts(request.query.name));
    }
    else {
      db.tareas.find({}, function(err, tareas) {
        if (err) {
            console.log (err);
            return res.reply('error');
        }
        request.reply(tareas);
    }
}


db.users.find({}, function(err, users) {
  if( err || !users) console.log("No users found");
    else
  {
    res.writeHead(200, {'Content-Type': 'application/json'});
    str='[';
    users.forEach( function(user) {
      str = str + '{ "name" : "' + user.username + '"},' +'\n';
    });
    str = str.trim();
    str = str.substring(0,str.length-1);
    str = str + ']';
    res.end( str);
  }
*/
