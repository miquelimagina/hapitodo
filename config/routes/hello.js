module.exports = [
  {
    method: 'GET',
    path:'/hello',
    config: {
      tags: ['api'],
      /*validate: {
        payload: Joi.object().keys({ name: Joi.string(), file: Joi.object().meta({ swaggerType: 'file' }) })
      },*/
    },
    handler: function (request, reply) {
        return reply('El aliento de mi gato huele a comida de gato');
    }
  },
];
