'use strict';
//var tarea = require('../../models/tarea');
var Datastore = require('nedb');
var Boom = require('boom');
var db = {};
db.tareas = new Datastore({ filename: __dirname + '/../db.json', autoload: true });

var Joi = require('joi');
/*
var tareaSchema = Joi.object().keys({
    title:        Joi.string().alphanum().min(3).max(70).required(),
    description:  Joi.string().alphanum().min(3).max(140).required(),
    status:       Joi.string().required().valid(['todo', 'doing', 'review','done']),
    tags:         Joi.array().items(Joi.string().required()),
    //createdBy:    Joi.date().max('now'),
    createdAt  :  Joi.date().max('now'),
    updateddAt  : Joi.date().max('now'),
});*/

/**
 * Getters & Setters
 */
var getTags = function (tags) {
  return tags.join(',');
};
var setTags = function (tags) {
  return tags.split(',');
};

function getTareas(request, reply){
  console.log('GET#: Tareas');
  if (request.query.title) {
    console.log("request.query:"+request.query);
    db.tareas.find({title:'/'+request.query.title+'/'}, function (err, tarea) {
      console.log('findTarea title');
      return reply(tarea);
    });
  }else{
    db.tareas.find({}, function (err, tareas) {
      return reply(tareas);
    });
  }

}

/**
 * Tarea methods
 */
function getTarea(request, reply) {
  console.log('GET#: Tarea id('+request.params.id+')');
  db.tareas.findOne({_id:request.params.id}, function (err, tarea) {
    if(Object.keys(tarea).length){  //count object length
      return reply(tarea);
    }else{
      return reply(Boom.notFound('Not Found'));
    }
  });
//return reply('gettarea');
}

function addTarea(request, reply) {
  console.log('POST#: ('+request.params+')');
    var tarea = {
        title:      request.payload.title,
        description:request.payload.description,
        status:     request.payload.status,
        tags:       request.payload.tags,
        createdAt:  Date.now()
    };

    db.tareas.insert(tarea, function (err, newTarea) {
      return reply(newTarea).created('/tareas/' + newTarea._id);
    });
}

function putTarea(request,reply){
  db.tareas.findOne({_id:request.params.id}, function (err, tarea){
    console.log(tarea);
    if(tarea && tarea !== "null" && tarea !== "undefined"){  //count object length (update tarea)
      console.log('PUT# Tarea id('+request.params.id+') exists:'+Object.keys(tarea).length);
      var dateCreatedAt=new Date(parseInt(tarea.createdAt));

      console.log(tarea);
      console.log(tarea['createdAt']);
      console.log(dateCreatedAt);
      console.log(dateCreatedAt.getTime());
      console.log('-----');

      var putTarea = {
        _id:        request.params.id,
        title:      request.payload.title,
        description:request.payload.description,
        status:     request.payload.status,
        tags:       request.payload.tags,
        createdAt:  dateCreatedAt.getTime(),
        updatedAt:  Date.now()
      };

      console.log(putTarea);

      db.tareas.update({ _id: request.params.id }, putTarea, {}, function (err, tareaReplaced) {
        // numReplaced = 1
        // The doc #3 has been replaced by { _id: 'id3', planet: 'Pluton' }
        // Note that the _id is kept unchanged, and the document has been replaced
        // (the 'system' and inhabited fields are not here anymore)
        //return reply(tareaReplaced).created('/tareas/' + tareaReplaced._id);
        return reply(putTarea);
      });//db.tareas.update

    }else{
      console.log('PUT# new tarea id('+request.params.id+')');
      var putNewTarea = {
        _id:        request.params.id,
        title:      request.payload.title,
        description:request.payload.description,
        status:     request.payload.status,
        tags:       request.payload.tags,
        createdAt:  Date.now()
      };

      db.tareas.insert(putNewTarea, function (err, newTarea) {
        return reply(newTarea).created('/tareas/' + newTarea._id);
      });//db.tareas.insert
    }//objects.key

  });//find
}//()


function removeTarea(request,reply){
  db.tareas.remove({_id:request.params.id}, {}, function (err, numRemoved) {
    console.log(numRemoved);
    if (numRemoved){
      return reply('tarea removed');
    }else{
      return reply(Boom.notFound('missing'));
    }
  });
}
/*
module.exports = [
  /*
  {
    method: 'GET',
    path:'/hello',
    config: {
      tags: ['api'],
      //validate: {
      //  payload: Joi.object().keys({ name: Joi.string(), file: Joi.object().meta({ swaggerType: 'file' }) })
      //},
    },
    handler: function (request, reply) {
        return reply('El aliento de mi gato huele a comida de gato');
    }
  },
  { method: 'GET', path: '/tareas', handler: internals.getTareas},
  { method: 'GET', path: '/tareas/{id}', internals.getTareas }
];*/


module.exports = [{
    method: 'GET',
    path: '/tareas',
    config: {
      tags: ['api'],
      validate: {
          query: {
              title: Joi.string()
          }
      },
      handler: getTareas
    }
}, {
    method: 'GET',
    path: '/tareas/{id}',
    config: {
      tags: ['api'],
      validate: {
          query: { id: Joi.string() }
      },
    handler: getTarea
  }
}, {
    method: 'POST',
    path: '/tareas',
    config: {
      tags: ['api'],
      validate: {
          payload: {
            title:      Joi.string().min(3).max(70).required(),
            description:Joi.string().min(3).max(140),
            status:     Joi.string().valid(['todo', 'doing', 'review','done']).required(),
            tags:       Joi.array().items(Joi.string().required())
          }
      },
      handler: addTarea
    }
}, {
    method: 'PUT',
    path: '/tareas/{id}',
    config: {
      tags: ['api'],
      validate: {
          payload: {
            title:      Joi.string().min(3).max(70).required(),
            description:Joi.string().min(3).max(140),
            status:     Joi.string().required().valid(['todo', 'doing', 'review','done']),
            tags:       Joi.array().items(Joi.string().required())
          }
      },
      handler: putTarea
    }
}, {
    method: 'DELETE',
    path: '/tareas/{id}',
    config: {
      tags: ['api'],
      handler: removeTarea
    }
}];



/*
function getTareas(request,reply){
  return reply('get tareas');
}
*/

