'use strict';

const Hapi = require('hapi');
var Good = require('good');
var routes = require('./config/routes');
var Types = require('hapi').Types;


// You can issue commands right away


// Create a server with a host and port
var port =  3000;
const server = new Hapi.Server();
server.connection({
    host: 'localhost', //'current_local_ip' || 'localhost'
    port: port,
    labels: ['api']
});
//Heroku hapi server: https://github.com/geek/hapi-heroku/blob/master/main.js
//const server =new Hapi.Server(+process.env.PORT, '0.0.0.0');

//routes in /config/routes
server.route(routes);

//Register good-console
server.register({
    register: Good,
    options: {
        reporters: [{
            reporter: require('good-console'),
            events: {
                response: '*',
                log: '*'
            }
        }]
    }
}, function (err) {
    if (err) {
        throw err; // something bad happened loading the plugin
    }
});

//Register hapi-swaggered && hapi-swaggered-ui for /docs
//  https://github.com/z0mt3c/hapi-swaggered
//  https://github.com/z0mt3c/hapi-swaggered-ui

server.register([
  require('inert'),
  require('vision'),
  {
    register: require('hapi-swaggered'),
    options: {
      tags: {
        'foobar/test': 'Example foobar description'
      },
      info: {
        title: 'Example API',
        description: 'Powered by node, hapi, joi, hapi-swaggered, hapi-swaggered-ui and swagger-ui',
        version: '1.0'
      }
    }
  },
  {
    register: require('hapi-swaggered-ui'),
    options: {
      title: 'Example API',
      path: '/docs',

      //authorization: {
      //  field: 'apiKey',
      //  scope: 'query', // header works as well
      //  valuePrefix: 'bearer '// prefix incase
      //  defaultValue: 'demoKey',
      //  placeholder: 'Enter your apiKey here'
      //},
      swaggerOptions: {
        validatorUrl: null
      }
    }
  }], {
    select: 'api'
  }, function (err) {
  if (err) {
    throw err;
  }
// '/'' to '/docs' route
  server.route({
    path: '/',
    method: 'GET',
    handler: function (request, reply) {
      reply.redirect('/docs');
    }
  });

/*
  server.start(function () {
    console.log('started on http://localhost:8000')
  })*/

// Start the server

  server.start((err) => {
    if (err) {
        throw err;
    }
    console.log('Server running at:', server.info.uri);
  });
});
//
//
/*
server.start(function () {
  if (err) {
        throw err;
    }

    console.log('Server started at [' + server.info.uri + ']');
});
*/
/*
server.register(require('hapi-heroku-helpers'), function (err) {

    // Assuming no err, start server

    server.start(function () {
        // ..
    });
});
*/
